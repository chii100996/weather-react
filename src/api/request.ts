export const baseUrl = process.env['REACT_APP_WEATHER_API_URL'];
const API_KEY = process.env['REACT_APP_WEATHER_API_KEY'];

export const request = async (path: string, params: any) => {
  const url = baseUrl + path + `&appid=${API_KEY}&units=metric`;
  const response = await fetch(url, {
    ...params,
  });
  const data = await response.json();
  return data;
};
